
package operatordemo;

import java.util.Scanner;

public class AssingmentOperator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        int a,b;
        System.out.print("Enter first int value : ");
        a = scanner.nextInt();
        
        System.out.print("Enter second int value : ");
        b = scanner.nextInt();
        a += b; // a= a+b;
        System.out.println("Sum = "+a);

        a -= b;
        System.out.println("Sub = "+a);

        a *= b;
        System.out.println("Mul = "+a);

        a /= b;
        System.out.println("Div = "+a);

        a %= b;
        System.out.println("Reminder = "+a);
        
        
    }
}
